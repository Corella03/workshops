<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;

class StudentsController extends Controller
{
    public function store(Request $request)
    {
        $students = new Students();
        $students->fill($request->all());
        $saved = $students->save();
        return response()->json(['details'=>$students],200);
    }
    public function show()
    {
        $students = Students::all();
        return response()->json(['details'=>$students],200);
    }
    public function show_by_id($id)
    {
        $students = \DB::table('students')->where('id', $id)->get();
        return response()->json(['details'=>$students],200);
    }

    public function destroy($id)
    {
        \DB::table('students')->where('id', $id)->delete();
        $students = Students::all();
        return response()->json(['details'=>$students],200);
    }
 
    public function update(Request $request, $id)
    { 
        if($request['firstname'] != null){
            \DB::table('students')->where('id', $id)->update(['firstname' => $request->firstname]);
        }
        if ($request['lastname'] != null) {
            \DB::table('students')->where('id', $id)->update(['lastname' => $request->lastname]);
        }
        if ($request['email'] != null) {
            \DB::table('students')->where('id', $id)->update(['email' => $request->email]);
        }
        if ($request['address'] != null) {
            \DB::table('students')->where('id', $id)->update(['address' => $request->address]);
        }
        $student = \DB::table('students')->where('id', $id)->get();
        return response()->json(['details'=>$students], 200);
    }
}
