<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware(['basicAuth'])->group(function () {
    Route::post('/students', 'StudentsController@store');
    Route::get('/students','StudentsController@show'); 
    Route::get('/students/{id}', 'StudentsController@show_by_id');
    Route::put('/students/{id}', 'StudentsController@update');
    Route::delete('/students/delete/{id}', 'StudentsController@destroy');
});
